package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;

import main.ChatController;

public class GUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField chatFeld;
	private JList<String> users;
	private JList<String> chat;
	private DefaultListModel<String> chatModel;
	private JPanel panel;
	private JLabel lblAdresse;
	private JTextField adressFeld;
	private JLabel lblPort;
	private JFormattedTextField portFeld;
	private JLabel lblName;
	private JTextField nameFeld;
	private ChatController controller;
	private JButton btnSenden;
	private JButton btnDisconnect;

	public GUI(ChatController contr) {
		controller = contr;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 340);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, 0.5, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridheight = 2;
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);

		lblAdresse = new JLabel("Adresse:");
		panel.add(lblAdresse);

		adressFeld = new JTextField();
		panel.add(adressFeld);
		adressFeld.setColumns(10);

		lblPort = new JLabel("Port");
		panel.add(lblPort);

		portFeld = new JFormattedTextField();
		panel.add(portFeld);
		portFeld.setValue(12345);

		lblName = new JLabel("Name");
		panel.add(lblName);

		nameFeld = new JTextField();
		panel.add(nameFeld);
		nameFeld.setColumns(10);

		JButton btnConnect = new JButton("Connect");
		GridBagConstraints gbc_btnConnect = new GridBagConstraints();
		gbc_btnConnect.fill = GridBagConstraints.BOTH;
		gbc_btnConnect.insets = new Insets(0, 0, 5, 0);
		gbc_btnConnect.gridx = 1;
		gbc_btnConnect.gridy = 0;
		contentPane.add(btnConnect, gbc_btnConnect);

		JLabel lblChat = new JLabel("Chat:");
		GridBagConstraints gbc_lblChat = new GridBagConstraints();
		gbc_lblChat.anchor = GridBagConstraints.WEST;
		gbc_lblChat.insets = new Insets(0, 0, 5, 5);
		gbc_lblChat.gridx = 0;
		gbc_lblChat.gridy = 2;
		contentPane.add(lblChat, gbc_lblChat);

		JLabel lblUser = new JLabel("User:");
		GridBagConstraints gbc_lblUser = new GridBagConstraints();
		gbc_lblUser.anchor = GridBagConstraints.WEST;
		gbc_lblUser.insets = new Insets(0, 0, 5, 0);
		gbc_lblUser.gridx = 1;
		gbc_lblUser.gridy = 2;
		contentPane.add(lblUser, gbc_lblUser);

		JScrollPane scp = new JScrollPane();
		chat = new JList<>();
		GridBagConstraints gbc_chat = new GridBagConstraints();
		gbc_chat.insets = new Insets(0, 0, 5, 5);
		gbc_chat.fill = GridBagConstraints.BOTH;
		gbc_chat.gridx = 0;
		gbc_chat.gridy = 3;
		contentPane.add(scp, gbc_chat);

		chatModel = new DefaultListModel<>();
		chat.setModel(chatModel);
		scp.setViewportView(chat);

		users = new JList<>();
		GridBagConstraints gbc_users = new GridBagConstraints();
		gbc_users.insets = new Insets(0, 0, 5, 0);
		gbc_users.fill = GridBagConstraints.BOTH;
		gbc_users.gridx = 1;
		gbc_users.gridy = 3;

		JScrollPane scp2 = new JScrollPane();
		scp2.setViewportView(users);
		contentPane.add(scp2, gbc_users);

		chatFeld = new JTextField();
		GridBagConstraints gbc_chatFeld = new GridBagConstraints();
		gbc_chatFeld.insets = new Insets(0, 0, 0, 5);
		gbc_chatFeld.fill = GridBagConstraints.HORIZONTAL;
		gbc_chatFeld.gridx = 0;
		gbc_chatFeld.gridy = 4;
		contentPane.add(chatFeld, gbc_chatFeld);
		chatFeld.setColumns(10);

		Action senden = new SendenAction("Senden");
		contentPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "senden");
		contentPane.getActionMap().put("senden", senden);

		btnSenden = new JButton(senden);
		btnSenden.setEnabled(false);
		GridBagConstraints gbc_btnSenden = new GridBagConstraints();
		gbc_btnSenden.fill = GridBagConstraints.BOTH;
		gbc_btnSenden.gridx = 1;
		gbc_btnSenden.gridy = 4;
		contentPane.add(btnSenden, gbc_btnSenden);

		btnDisconnect = new JButton("Disconnect");
		btnDisconnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controller.disconnect();
				btnSenden.setEnabled(false);
				btnDisconnect.setEnabled(false);
				users.setModel(new DefaultListModel<>());// userliste leeren
			}
		});
		btnDisconnect.setEnabled(false);
		GridBagConstraints gbc_btnDisconnect = new GridBagConstraints();
		gbc_btnDisconnect.fill = GridBagConstraints.BOTH;
		gbc_btnDisconnect.insets = new Insets(0, 0, 5, 0);
		gbc_btnDisconnect.gridx = 1;
		gbc_btnDisconnect.gridy = 1;
		contentPane.add(btnDisconnect, gbc_btnDisconnect);

		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String adr = adressFeld.getText();
				int port = (int) portFeld.getValue();
				String name = nameFeld.getText();
				InetAddress addr;
				try {
					addr = InetAddress.getByName(adr);
					controller.connectToServer(name, addr, port);
					btnSenden.setEnabled(true);
					btnDisconnect.setEnabled(true);
					chatModel = new DefaultListModel<>();// chatverlauf leeren
					chat.setModel(chatModel);
				} catch (UnknownHostException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null,
							"Der Host konnte nicht gefunden werden:\n" + e.getLocalizedMessage(), "Fehler",
							JOptionPane.ERROR_MESSAGE);
				} catch (IOException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, e.getLocalizedMessage(), e.getClass().getSimpleName(),
							JOptionPane.ERROR_MESSAGE);
					btnSenden.setEnabled(controller.isConnected());
					btnDisconnect.setEnabled(controller.isConnected());
				}
			}
		});
	}

	/**
	 * Aktualisiert die Userliste
	 * 
	 * @param userNames
	 *            Liste der Usernamen
	 */
	public void updateUsers(String[] userNames) {
		DefaultListModel<String> usersModel = new DefaultListModel<>();
		for (String name : userNames) {
			usersModel.addElement(name);
		}
		users.setModel(usersModel);
	}

	/**
	 * F�gt dem Chatverlauf eine neue Nachricht hinzu.
	 * 
	 * @param name
	 *            Der Autor der Nachricht
	 * @param msg
	 *            Die Nachricht
	 */
	public void putMessage(String name, String msg) {
		String str = name + ": " + msg;
		chatModel.addElement(str);
	}

	private final class SendenAction extends AbstractAction {
		private static final long serialVersionUID = 1L;

		public SendenAction(String str) {
			super(str);
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String text = chatFeld.getText();
			System.out.println("senden");
			if (!text.isEmpty() && controller.isConnected()) {
				controller.sendMessage(text);
				chatFeld.setText("");
			}
		}
	}

	/**
	 * Informiert den Nutzer �ber einen aufgetretenen Fehler
	 * 
	 * @param string
	 *            Die Fehlermeldung
	 */
	public void informUser(String string) {
		JOptionPane.showMessageDialog(null, string, "Error", JOptionPane.ERROR_MESSAGE);
		btnSenden.setEnabled(controller.isConnected());
		btnDisconnect.setEnabled(controller.isConnected());
	}

	public char[] requestPassword() {
		String pw=JOptionPane.showInputDialog("Passwort ben�tigt:");
		return pw.toCharArray();
	}
}
