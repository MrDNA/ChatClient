package net;

public class Puffer {

	private int index=0;
	private byte[] data;
	
	public Puffer() {
		this(256);
	}

	public Puffer(int initialSize) {
		data=new byte[initialSize];
	}
		
	public void add(byte[] bytes, int length) {
		if(length+index>data.length) {
			resize();
		}
		for (int i = 0; i < length; i++) {
			data[index++]=bytes[i];
		}
	}
	
	private void resize() {
		byte[] newArray=new byte[data.length<<1];
		for (int i = 0; i < index; i++) {
			newArray[i]=data[i];
		}
		data=newArray;
	}
	
	public byte[] getData() {
		byte[] bytes=new byte[index];
		for (int i = 0; i < bytes.length; i++) {
			bytes[i]=data[i];
		}
		return bytes;
	}
}
