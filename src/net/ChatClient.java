package net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import main.ChatController;

public class ChatClient extends Thread{
	

	private static final int keyDerivationIterationCount=20000;
	private static final int BUFFER_SIZE = 256;
	private ChatController controller;
	protected boolean connected=false,disconnected=false;
	private InputStream in;
	private OutputStream out;
	private Socket socket;
	private String displayName;
	protected EncryptionHandler enc=null;
	
	public ChatClient(ChatController controller) {
		this.controller=controller;
	}

	/**
	 * Setzt den Namen mit dem sich am Server angemeldet werden soll.
	 * Der Name darf max. 256 Zeichen lang sein. Falls der Name l�nger ist, wird er gek�rtzt.
	 * 
	 * @param name Der zu verwendende Name
	 */
	public void setDisplayName(String name) {
		if(name.length()>256)
			displayName=name.substring(0,256);
		else
			displayName=name;
	}
	 /**
	  * Verbindet sich mit dem angegebenen Server und meldet sich an.
	  * 
	  * @param adrr Die Adresse des Servers
	  * @param port Der Port des Servers
	  * @throws IOException falls was schief l�uft
	  */
	public void connect(InetAddress adrr,int port) throws IOException {
		socket=new Socket(adrr, port);
		in=socket.getInputStream();
		out=socket.getOutputStream();
		byte[] namebytes;
		try {
			namebytes=displayName.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			namebytes=displayName.getBytes();
		}
		out.write(namebytes);
		connected=true;
	}
	
	@Override
	public synchronized void start() {
		if(connected)//wenn man nicht verbunden ist braucht man nicht starten
			super.start();
		else
			System.err.println("nicht verbunden");
	}
	
	@Override
	public void run() {
		while(connected) {//solange man verbunden ist, Nachrichten lesen
			try {
				Puffer puffer=new Puffer(256);
				int read;
				byte[] bytes=new byte[BUFFER_SIZE];
				do {
					read=in.read(bytes);
					puffer.add(bytes,read);
				}while(read>=BUFFER_SIZE);
				processMsg(puffer.getData());
				
			} catch (IOException e) {//bei Fehlern, Verbindung trennen
				handleException(e);
			}
		}
	}
	
	/**
	 * Verabeitet die empfangene Nachricht und f�hrt die n�tigen Operationen aus. 
	 * 
	 * @param msg Die empfangene Nachricht
	 */
	protected void processMsg(byte[] bytes) {
		String msg;
		try {
			msg = new String(bytes,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			msg=new String(bytes);
		}
		System.out.println("empfange "+msg);
		String type=msg.substring(0,4);//Die ersten vier Zeichen geben an, was zu tun ist.
		switch (type) {
		case "MSG\0"://neue Nachricht
			int pos=msg.substring(4).indexOf('\0');
			controller.newMessage(msg.substring(4, pos+4),msg.substring(pos+4));
			break;
		case "UPD\0"://Update
			controller.updateUsers(msg.substring(4).split("\0"));
			break;
		case "INI\0"://Der Server k�ndigt eine Verschl�sselung an
			byte[] salt=new byte[bytes.length-4];
			for (int i = 0; i < salt.length; i++) {
				salt[i]=bytes[i+4];
			}
			System.out.println("salt: "+new String(salt));
			char[]pw=controller.requestPassword();
			if(pw!=null) {
				enc=new EncryptionHandler(pw, salt);
			}
			else {
				disconnect();
				controller.confirmDisconnect("Password ben�tigt!");
			}
			break;
		case "SEC\0"://Die Nachricht ist verschl�sselt
			byte[] data=new byte[bytes.length-4];
			for (int i = 0; i < data.length; i++) {
				data[i]=bytes[i+4];
			}
			data=enc.decrypt(data);
			
			processMsg(data);
			break;

		default:
			System.err.println("Unbekannter Code: "+type);
			break;
		}
	}
	
	/**
	 * Sendet eine Nachricht an den Server.
	 * Schlie�t im Fehlerfall die Verbindung
	 * 
	 * @param msg Die zu sendende Nachricht
	 */
	public void sendMessage(String msg)  {
		try {
			byte[] data;
			try {
				data=msg.getBytes("UTF-8");
			}
			catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				data=msg.getBytes();
			}
			if(enc!=null) {
				data=enc.encrypt(data);
			}
			System.out.println("sende "+new String(data));
			out.write(data);
		}
		catch (IOException e) {
			handleException(e);
		}
	}
	
	public boolean isConnected() {
		return connected;
	}

	public boolean isDisconnected() {
		return disconnected;
	}
	
	protected void handleException(Exception e) {
		e.printStackTrace();
		if(!e.getMessage().equals("Socket closed")) {//Verbindung wurde nicht vom Client geschlossen
			disconnect();
			controller.confirmDisconnect(e);
		}
	}
	
	/**
	 * Schlie�t die Verbindung
	 */
	public void disconnect() {
		connected=false;
		disconnected=true;
		try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected class EncryptionHandler{
		
		private SecretKeySpec secretKey;

		public EncryptionHandler(char[] password, byte[] salt) {
			try {
			SecretKeyFactory factory=SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
			System.out.println("Generiere Schl�ssel...");
			KeySpec spec=new PBEKeySpec(password, salt, keyDerivationIterationCount, 256);
			SecretKey tmp = factory.generateSecret(spec);
			secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");
			System.out.println("erfolgreich");

			}
			catch (GeneralSecurityException e) {
				e.printStackTrace();
			}
		}
		
		public byte[] decrypt(byte[]data){
			try {
				System.out.println("decrypting");
				Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
				int ivlen=cipher.getParameters().getParameterSpec(IvParameterSpec.class).getIV().length;
				System.out.println("iv length: "+ivlen);
				byte[]iv =new byte[ivlen];
				byte[]ciphertext=new byte[data.length-ivlen];
				System.out.println("cipherlen: "+ciphertext.length);
				for (int i = 0; i < iv.length; i++) {
					iv[i]=data[i];
				}
				int j=ivlen;
				for (int i = 0; i < ciphertext.length; i++) {
					ciphertext[i]=data[j];
					j++;
				}
				cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
				return cipher.doFinal(ciphertext);
				
			} catch (GeneralSecurityException e) {
				e.printStackTrace();
				return data;
			}
			
			
		}
		
		public byte[] encrypt(byte[] data){
			try {
				System.out.println("encrypting");
				Cipher cipher=Cipher.getInstance("AES/CBC/PKCS5Padding");
				cipher.init(Cipher.ENCRYPT_MODE, secretKey);
				AlgorithmParameters params = cipher.getParameters();
				byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
				System.out.println("iv length: "+iv.length);
				byte[] ciphertext = cipher.doFinal(data);
				System.out.println("encrypted");
				byte[]msg=new byte[4+iv.length+ciphertext.length];
				msg[0]='S';msg[1]='E';msg[2]='C';msg[3]='\0';
				int i=4;
				for (int j = 0; j < iv.length; j++) {
					msg[i]=iv[j];
					i++;
				}
				for (int j = 0; j < ciphertext.length; j++) {
					msg[i]=ciphertext[j];
					i++;
				}
				System.out.println("cipherlength: "+ciphertext.length);
				return msg;
				
			} catch (GeneralSecurityException e) {
				e.printStackTrace();
				return data;
			}
		}
	}
	
}
