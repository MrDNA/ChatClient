package main;

import java.io.IOException;
import java.net.InetAddress;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import gui.GUI;
import net.ChatClient;

public class ChatController {

	private ChatClient client;
	private GUI gui;
	
	public static void main(String[] args) {
		new ChatController();
	}
	
	public ChatController() {
		gui = new GUI(this);
		
		try {//f�rs bessere Aussehen
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		gui.setVisible(true);
	}

	/**
	 * Erzeugt einen Client und verbindet ihn zu einem Server.
	 * Schlie�t eine eventuell vorhande Verbindung
	 * 
	 * @param name Der Name mit dem sich am Server angemeldet werden soll
	 * @param addr Die Adresse des Servers
	 * @param port Der Port des Servers
	 * @throws IOException falls was schief l�uft
	 */
	public void connectToServer(String name,InetAddress addr,int port) throws IOException {
		if(client!=null&&client.isConnected()) {
			client.disconnect();
		}
		client=new ChatClient(this);
		client.setDisplayName(name);
		client.connect(addr, port);
		client.start();
	}
	
	/**
	 * Schlie�t die Verbindung zum Server, falls vorhanden
	 */
	public void disconnect() {
		if(client!=null&&client.isConnected()) {
			client.disconnect();
		}
	}
	
	/**
	 * Zeigt an erhaltene Nachricht in der GUI an.
	 * 
	 * @param name Autor der Nachricht
	 * @param msg Die Nachricht
	 */
	public void newMessage(String name,String msg) {
		gui.putMessage(name, msg);
	}
	
	/**
	 * Aktualisiert die Liste der Nutzer in der GUI
	 * 
	 * @param users
	 */
	public void updateUsers(String[] users) {
		gui.updateUsers(users);
	}

	/**
	 * Sendet eine Nachricht an den Server
	 * 
	 * @param msg Die Nachricht
	 * @throws IOException falls was schief l�uft
	 */
	public void sendMessage(String msg) {
		client.sendMessage(msg);
	}
	
	/**
	 * 
	 * @return true, falls der Client mit einem Server verbunden ist.
	 */
	public boolean isConnected() {
		return client!=null&&client.isConnected();
	}

	
	/**
	 * informiert den Nutzer das die Verbindung getrennt wurde
	 */
	public void confirmDisconnect(Exception grund) {
		gui.informUser("Disconnected:\n"+grund.getLocalizedMessage());
	}
	
	/**
	 * informiert den Nutzer das die Verbindung getrennt wurde
	 */
	public void confirmDisconnect(String grund) {
		gui.informUser("Disconnected:\n"+grund);
	}
	
	
	public char[] requestPassword() {
		return gui.requestPassword();
	}
}
